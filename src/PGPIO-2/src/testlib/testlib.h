#pragma once

namespace testlib {

inline int synthetic_function()
{
    return 5;
}

struct synthetic_class
{
    int test();
};

}