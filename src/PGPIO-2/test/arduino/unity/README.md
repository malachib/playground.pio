# Notes

* Specifying `lib/pgpio-2` symlink does successfully yank in that library, but results
  in `unity` dependency getting clobbered
* Specifying `lib/pgpio-2` as an actual folder can work and does not interrupt unity
  dependency, when constructed as:
    * `lib/pgpio-2/src` as a symlink and
    * `lib/pgpio-2/library.json` as a symlink
