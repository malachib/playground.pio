#include <testlib/testlib.h>

#include <unity.h>

static void test1()
{
    TEST_ASSERT_EQUAL_INT(5, testlib::synthetic_function());
}

void test_synthetic()
{
    RUN_TEST(test1);
}
