# Diagnostic Project

This project exists to attempt to clean, mitigate, repair or otherwise motivate into
functionality the cross referencing of `src` folder from various test programs under
`test` folder

## Structure

### testlib

Nothing unusual here, the code under `src` folder represents a purely synthetic library
which we test against

### test/arduino

Collection of platformio specific code, a mix of regular projects acting as test projects
as well as a proper unit test project

#### test/arduino/unity

This is the platformio specific unit test project to test against testlib and below-mentioned
shared unity folder

### test/unity

An "out of band" location for the core unit tests themselves.  This location is a neutral
non-arduino/non-pio location.  In non synthetic scenarios, many other frameworks pull from
this folder (i.e. esp-idf, VisualDSP++, more)

