#include <Arduino.h>

#include <estd/ostream.h>
#include <estd/thread.h>

using namespace estd::chrono_literals;

estd::arduino_ostream cout(Serial);

constexpr int LED_IO = 13;

void setup()
{
    Serial.begin(115200);

    pinMode(LED_IO, OUTPUT);
}


void loop()
{
    static bool state = false;

    digitalWrite(LED_IO, state = !state);

    cout << F("state = ") << state << estd::endl;

    estd::this_thread::sleep_for(500ms);
}