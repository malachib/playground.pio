#include "lib_b.h"

#ifdef USE_LIB_A

#include "lib_a.h"

int get_setting()
{
    return LIB_A_DEFAULT_SETTING;
}

#else

int get_setting()
{
    return 0;
}

#endif