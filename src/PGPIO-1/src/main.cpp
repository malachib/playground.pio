#include <iostream>

#include <lib_a.h>
#include <lib_b.h>
//#include <conf/lib_a_config.h>

int main()
{
    std::cout << "Hello World!" << std::endl;
    std::cout << "Got setting: " << get_setting() << std::endl;
    return 0;
}